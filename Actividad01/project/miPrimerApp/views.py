from django.shortcuts import render, HttpResponse
# Importa los modelos
from .models import *

from django.db.models import Count

# Create your views here.
def index(request):
    # Query, `get` solo se usa cuando sabemos con certeza que el objeto esta en la tabla
    #   de otro modo se usa `filter`
    group1_students = Estudiante.objects.filter(grupo= 1) # Todos los estudiantes del grupo 1
    group4_students = Estudiante.objects.filter(grupo= 4) # Todos los estudiantes del grupo 4

    # Cuenta los apellidos y forma una lista con los que se repiten
    dupes = Estudiante.objects.values('apellidos').annotate(Count('pk')).filter(pk__count__gt= 1)
    # Nueva query filtrando los apellidos duplicados obtenidos. Ordena alfabeticamente
    same_lastname_students = Estudiante.objects.order_by('apellidos').filter(apellidos__in= [student['apellidos'] for student in dupes])

    # Cuenta los apellidos y forma una lista con los que se repiten
    dupes = Estudiante.objects.values('edad').annotate(Count('edad')).filter(edad__count__gt= 1)
    # Nueva query filtrando los apellidos duplicados obtenidos. Ordena alfabeticamente
    same_age_students = Estudiante.objects.order_by('edad').filter(edad__in= [student['edad'] for student in dupes])

    # De la tabla `same_age_students` obten los que sean del grupo 3
    group3_sameage = same_age_students.filter(grupo= 3)


    # Todos los estudiantes ordenados por grupo
    all_students = Estudiante.objects.order_by('grupo').all()


    # Contexto
    context = {
        'estudiantes_g1' : group1_students,
        'estudiantes_g4' : group4_students,
        'mismo_apellido' : same_lastname_students,
        'misma_edad'     : same_age_students,
        'g3_misma_edad'  : group3_sameage,
        'estudiantes'    : all_students
    }

    # Imprime query en terminal
    print(group1_students)

    return render(request, 'index.html', context)